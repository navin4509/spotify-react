import React, { useState } from "react";
import { Link } from "react-router-dom";
import { auth } from "../Api/firebase";
import {
  createUserWithEmailAndPassword,
  sendEmailVerification,
  updateProfile,
} from "firebase/auth";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

export default function Signup() {
  let navigate = useNavigate();
  let [name, setName] = useState();
  let [email, setEmail] = useState();
  let [password, setPassword] = useState();
  let [confirmPassword, setConfirmPassword] = useState();

  let handlesignUp = async (e) => {
    e.preventDefault();
    console.log(name, email, password, confirmPassword);
    try {
      if (password !== confirmPassword) {
        toast.error("Password Mismatched");
      } else {
        let userData = await createUserWithEmailAndPassword(
          auth,
          email,
          password
        );
        toast.success("User Created Succesfully");
        console.log(userData);
        let user = userData.user;
        sendEmailVerification(user);
        toast.success(`Email Verification Sent ${email}`);
        updateProfile(user, { displayName: name });
        navigate("/login");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="container mt-5">
        <form className=" m-auto w-50 p-5" onSubmit={handlesignUp}>
          <div>
            <h2 className="text-center" style={{ fontWeight: "bold" }}>
              Signup for free to start <br /> Listening.
            </h2>
            <hr className="text-white" />

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>What's your email?</label>
              <input
                className="form-control"
                type="text"
                placeholder="Enter Your Email"
                required
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>Create Password</label>
              <input
                className="form-control"
                type="password"
                placeholder="Create a Password"
                required
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>Confirm Password</label>
              <input
                className="form-control"
                type="password"
                placeholder="Confirm Your Password"
                required
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </div>

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>
                What should we call you?
              </label>
              <input
                className="form-control"
                type="text"
                placeholder="Enter A Profile Name"
                required
                onChange={(e) => setName(e.target.value)}
              />
              <label>This appears on your profile.</label>
            </div>

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>
                What's your date of birth?
              </label>
              <input className="form-control" type="date" />
            </div>

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>
                What's your mobile number?
              </label>
              <input
                className="form-control"
                type="number"
                placeholder="Enter Your Mobile Number"
              />
            </div>

            <div className="mt-3">
              <label style={{ fontWeight: "bold" }}>What's your gender?</label>
              <br />
              <input type="radio" name="gender" />
              <label className="mt-3 p-2">Male</label>
              <input type="radio" name="gender" />
              <label className="p-2">Female</label>
              <input type="radio" name="gender" />
              <label className="p-2">Non-Binary</label>
            </div>
            <div>
              <input type="checkbox" />
              <label className="m-2">
                I would prefer not to receive marketing messages from Spotify
              </label>
              <br />
              <input type="checkbox" />
              <label className="m-2">
                Share my registration data with Spotify's content providers for
                marketing purposes.
              </label>
            </div>
          </div>

          <div className="mt-4">
            <h6 className="text-center">
              By clicking on sign-up, you agree to Spotify's{" "}
              <Link to="/terms">Terms and Conditions of Use.</Link>
            </h6>
            <h6 className="text-center">
              To learn more about how Spotify collects, uses, shares and
              protects your personal data, please see{" "}
              <Link to="/privacy">Spotify's Privacy Policy.</Link>
            </h6>
          </div>

          <div className="mt-3 text-center">
            <input type="submit" className="btn btn-danger" />
          </div>
        </form>

        <h6 className="text-dark text-center">
          Already Have an account? <Link to="/login">Login here</Link>
        </h6>
      </div>
    </>
  );
}
