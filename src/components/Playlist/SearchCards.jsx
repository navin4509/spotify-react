import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./SearchCards.css"

export default function SearchCards() {
  let [data, setData] = useState([]);

  useEffect(() => {
    let dataUrl = `https://gist.githubusercontent.com/Navin4509/5c8014c246151dd385bf4b7a97950a93/raw/8813d96c92eca6fb6246435e19dfeb7ccea2d99e/gistfile1.txt`;
    axios
      .get(dataUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  return (
    <>
      <div className="main row">
        {data.map((datas) => {
          return (
            <div className="col">
              <div
                className="card-body bg-transparent text-center p-2 rounded"
                style={{
                  width: "200px",
                  height: "260px",
                  borderRadius: "10px",
                }}
              >
                <Link to={`/${datas.title}`} className="text-white">
                  <div className="hovering">
                    <img
                      className="rounded"
                      src={datas.image}
                      alt="images"
                      style={{ width: "100%", height: "200px" }}
                    />
                    <div className="middle">
                      <p className="card-title text-center mt-2 ">
                        {datas.title}
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
