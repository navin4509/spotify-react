import React from 'react'
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { BsPlayCircleFill } from "react-icons/bs";
import axios from "axios";
import "./Bollywood.css"

export default function Bollywood() {
    let [data, setData] = useState([]);

  useEffect(() => {
    let dataUrl = `https://gist.githubusercontent.com/Navin4509/af2988190401ae0f6828203b6831d171/raw/27befca17226a652afd5502f3bc6fde9ede463d3/gistfile1.txt`;
    axios
      .get(dataUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  return (
    <>
        <div className="row cards-container">
        {data.map((datas) => {
          return (
            <div className="col">
              <div
                className="card-body bg-transparent text-center p-2 rounded"
                style={{
                  width: "180px",
                  height: "260px",
                  borderRadius: "10px",
                }}
              >
                <Link to={`/${datas.title}`} className="text-white">
                  <div className="hovering">
                    <img
                      className="rounded"
                      src={datas.image}
                      alt="images"
                      style={{ width: "100%", height: "200px" }}
                    />
                    <div className="middle">
                      <i>
                        <BsPlayCircleFill />
                      </i>
                    </div>
                    <h6 className="card-title text-center mt-2">
                      {datas.title}
                    </h6>
                  </div>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </>
  )
}
