import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { BsPlayCircleFill } from "react-icons/bs";
import axios from "axios";
import "./Trending.css";

export default function Trending() {
  let [data, setData] = useState([]);

  useEffect(() => {
    let dataUrl = `https://gist.githubusercontent.com/Navin4509/461dd33744632a9ccf4ddbf69ce93d7b/raw/31984023e6775e8d2bd82f2fd25d2d1b9e2358b3/gistfile1.txt`;
    axios
      .get(dataUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  return (
    <>
      <div className="row cards-container">
        {data.map((datas) => {
          return (
            <div className="col">
              <div
                className="card-body bg-transparent text-center p-2 rounded"
                style={{
                  width: "180px",
                  height: "260px",
                  borderRadius: "10px",
                }}
              >
                <Link to={`/${datas.title}`} className="text-white">
                  <div className="hovering">
                    <img
                      className="rounded"
                      src={datas.image}
                      alt="images"
                      style={{ width: "100%", height: "200px" }}
                    />
                    <div className="middle">
                      <i>
                        <BsPlayCircleFill />
                      </i>
                    </div>
                    <h6 className="card-title text-center mt-2">
                      {datas.title}
                    </h6>
                  </div>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
