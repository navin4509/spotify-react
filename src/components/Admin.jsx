import React from "react";
import { NavLink } from "react-router-dom";

export default function Admin() {
  return (
    <>
      <div className="container mt-5 text-white text-center">
        <form className="card bg-dark m-auto w-50 p-3">
          <div>
            <h3>Admin Login</h3>
            <hr className="text-white" />

            <div>
              <input
                className="form-control"
                type="text"
                placeholder="Enter Your User ID"
              />
            </div>

            <div className="mt-3">
              <input
                className="form-control"
                type="text"
                placeholder="Enter Your Password"
              />
            </div>
          </div>
          <div className="mt-3">
            <input type="submit" className="btn btn-danger btn-sm" />
            <p className="mt-2">Forgot Password ?</p>
          </div>
        </form>
        <div className="d-flex mt-3 justify-content-center">
          <h6 className="text-dark"><NavLink to="/login">User Login</NavLink> /</h6>
          <h6 className="text-dark"><NavLink to="/signup">Create Your Account</NavLink></h6>
        </div>
      </div>
    </>
  );
}
