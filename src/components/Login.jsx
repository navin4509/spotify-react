import React, { useState } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { auth } from "../Api/firebase";
import { signInWithEmailAndPassword } from "firebase/auth";
import { NavLink } from "react-router-dom";

export default function Login() {

  let navigate = useNavigate();
  let [email, setEmail] = useState();
  let [password, setPassword] = useState();

  let handleLogin = async (e) => {
    e.preventDefault();
    let userData = await signInWithEmailAndPassword(auth, email, password);
    if (userData.user.emailVerified === true) {
      toast.success("User Logged In Succesfully");
      navigate("/");
    } else {
      toast.error("Please Check Email is Verified");
      toast.error("Please Provide Exact Email & Password");
    }
  };

  return (
    <>
      <div className="container mt-5 text-white text-center">
        <form className="card bg-dark m-auto w-50 p-3"  onSubmit={handleLogin}>
          <div>
            <h3>User Login</h3>
            <hr className="text-white" />

            <div>
              <input
                className="form-control"
                type="text"
                placeholder="Enter Your Email"
                required
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="mt-3">
              <input
                className="form-control"
                type="password"
                placeholder="Enter Your Password"
                required
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
          </div>
          <div className="mt-3">
            <input type="submit" className="btn btn-danger btn-sm" />
            <p className="mt-2">Forgot Password ?</p>
          </div>
        </form>
        <div className="d-flex mt-3 justify-content-center">
          <h6 className="text-dark"><NavLink to="/admin">Admin Login</NavLink>  /</h6>
          <h6 className="text-dark"><NavLink to="/signup">Create Your Account</NavLink></h6>
        </div>
      </div>
    </>
  );
}
