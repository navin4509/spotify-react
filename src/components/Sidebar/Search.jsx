import React from "react";
import { Link } from "react-router-dom";
import "./Search.css";
import SearchHome from "./SearchHome";

export default function Search() {
  return (
    <>
      <Link to="/search">
        <div className="form">
          <i className="fa fa-search"></i>
          <input
            type="text"
            className="form-control"
            placeholder="Artists, songs, or podcasts"
          />
        </div>
      </Link>
      <SearchHome />
    </>
  );
}
