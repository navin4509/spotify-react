import React from "react";
import "./SearchHome.css";
import SearchCards from "../Playlist/SearchCards";

export default function SearchHome() {
  return (
    <>
      <div className="main_container" style={{ backgroundColor: "#121212" }}>
        <div className="wrappers">
          <div className="sections">
            <h4 className="p-3 text-white">Browse all</h4>
            <SearchCards />
          </div>
        </div>
      </div>
    </>
  );
}
