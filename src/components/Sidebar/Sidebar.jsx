import React from "react";
import { NavLink } from "react-router-dom";
import { FaSpotify } from "react-icons/fa";
import { TiHome } from "react-icons/ti";
import { FiSearch } from "react-icons/fi";
import { BiLibrary } from "react-icons/bi";
import { MdPlaylistAdd } from "react-icons/md";
import { FcLike } from "react-icons/fc";
import "./Sidebar.css";

export default function Sidebar() {
  return (
    <>
      <div className="leftMenu h-100">
        <div className="logoContainer">
          <div className="logo">
            <i>
              <FaSpotify />
            </i>
            <NavLink to="/" className="nav-link">
              <h2>Spotify</h2>
            </NavLink>
          </div>
        </div>

        <div className="homeContainer">
          <div className="home">
            <i>
              <TiHome />
            </i>
            <NavLink to="/" className="nav-link">
              <p>Home</p>
            </NavLink>
          </div>
        </div>

        <div className="homeContainer">
          <div className="home">
            <i>
              <FiSearch />
            </i>
            <NavLink to="/search" className="nav-link">
              <p>Search</p>
            </NavLink>
          </div>
        </div>

        <div className="homeContainer">
          <div className="home">
            <i>
              <BiLibrary />
            </i>
            <NavLink to="/" className="nav-link">
              <p>Your Library</p>
            </NavLink>
          </div>
        </div>

        <div className="homeContainer">
          <div className="home">
            <i>
              <MdPlaylistAdd />
            </i>
            <NavLink to="/" className="nav-link">
              <p>Create Playlist</p>
            </NavLink>
          </div>
        </div>

        <div className="homeContainer">
          <div className="home">
            <i>
              <FcLike />
            </i>
            <NavLink to="/" className="nav-link">
              <p>Liked Songs</p>
            </NavLink>
          </div>
        </div>

        <div className="terms">
        <h6 className="text-white text-sm-left">Cookies</h6>
        <h6 className="text-white text-sm-left">Privacy</h6>
        </div>

      </div>
    </>
  );
}
