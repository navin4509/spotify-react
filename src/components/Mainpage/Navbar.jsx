import React from "react";
import { NavLink } from "react-router-dom";
import { BsChevronLeft } from "react-icons/bs";
import { BsChevronRight } from "react-icons/bs";
import {useNavigate} from "react-router-dom"
import "./Navbar.css";

export default function Navbar() {
  const navigate = useNavigate();
  return (
    <>
      <nav className="maincontainer navbar bg-dark navbar-dark navbar-expand-sm">
        <div className="container d-flex justify-content-end">
          <div>
            <ul className="navbar-nav">

              <i className="navigation1" onClick={()=>navigate(-1)}>
                <BsChevronLeft />
              </i>
              <i className="navigation2" onClick={()=>navigate(+1)}>
                <BsChevronRight />
              </i>

              <li className="nav-item">
                <NavLink to="/premium" className="nav-link">
                  Premium
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/support" className="nav-link">
                  Support
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/downloads" className="nav-link">
                  Downloads
                </NavLink>
              </li>
              <li className="text-white">
                <h3>|</h3>
              </li>
              <li className="nav-item">
                <NavLink to="/signup" className="nav-link">
                  Signup
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/login" className="nav-link">
                  Login
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
