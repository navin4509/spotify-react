import React from "react";
import { Outlet } from "react-router-dom";
import Bollywood from "../Playlist/Bollywood";
import Tamil from "../Playlist/Tamil";
import Telegu from "../Playlist/Telegu";
import Trending from "../Playlist/Trending";
import "./Home.css";

export default function Home() {
  return (
    <>
    
      <div className="main_container" style={{ backgroundColor: "#121212"}}>
        <div className="wrappers">
          <div className="sections">
              <h5 className="p-3 text-white"># Trending Songs</h5>
            <Trending />
              <h5 className="p-3 text-white"># Telegu Songs</h5>
            <Telegu />
              <h5 className="p-3 text-white"># Bollywood Songs</h5>
            </div>
            <Bollywood />
              <h5 className="p-3 text-white"># Tamil Songs</h5>
            <Tamil />
        </div>
      </div>
      <Outlet></Outlet>
    </>
  );
}
