import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Home from "./components/Mainpage/Home";
import Navbar from "./components/Mainpage/Navbar";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Admin from "./components/Admin";
import Sidebar from "./components/Sidebar/Sidebar";
import Search from "./components/Sidebar/Search";


const App = () => {
  return (
    <>
      <BrowserRouter>
        <div className="row">
          <Navbar />
      <ToastContainer pauseOnHover theme="dark"/>
          <div className="col-2">
            <Sidebar />
          </div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/search" element={<Search />} />
            <Route path="/login" element={<Login />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/signup" element={<Signup />} />
          </Routes>
        </div>
      </BrowserRouter>
    </>
  );
};

export default App;
