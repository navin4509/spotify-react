import React, { createContext, useEffect, useState } from 'react'
import {auth} from './firebase'
import {onAuthStateChanged} from 'firebase/auth'


export let usercontext = createContext()
export default function Authcontext({children}) {
    let [user, setUser] = useState();
    useEffect(()=>{
        return onAuthStateChanged(auth,userinfo=>{
            if(userinfo && userinfo.emailVerified === true){
                let Token= userinfo.accessToken;
                window.sessionStorage.setItem("Token",Token)
                setUser(userinfo)
            }else{
                window.sessionStorage.removeItem("Token")
                setUser(null);
            }
        })
    },[])
  return (
    <usercontext.Provider value={user}>
        {children}
    </usercontext.Provider>
  )
}
