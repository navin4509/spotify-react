// Import the functions you need from the SDKs you need
import {
    initializeApp
} from "firebase/app";
import {
    getAnalytics
} from "firebase/analytics";
import {
    getAuth
} from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyD61Tf-jr8BKYIF0ymfYKvKQwTQYBBC6xQ",
    authDomain: "moviestream-43f8d.firebaseapp.com",
    projectId: "moviestream-43f8d",
    storageBucket: "moviestream-43f8d.appspot.com",
    messagingSenderId: "455529262937",
    appId: "1:455529262937:web:8ff4ea1ffa91484e13a648",
    measurementId: "G-8RQ4MMBTSY"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const analytics = getAnalytics(firebase);

export let auth = getAuth(firebase);
export default firebase;